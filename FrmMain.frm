VERSION 5.00
Begin VB.Form FrmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Contabilidad - Soporte"
   ClientHeight    =   4875
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   12780
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4875
   ScaleWidth      =   12780
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Conta2 
      Caption         =   "Anulacion y Generacion"
      Height          =   3375
      Left            =   9120
      TabIndex        =   16
      Top             =   600
      Width           =   3495
      Begin VB.CommandButton cmdContabiliza2 
         Appearance      =   0  'Flat
         BackColor       =   &H000000FF&
         Caption         =   "Contabiliza"
         Height          =   495
         Left            =   720
         MaskColor       =   &H0000C000&
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   2400
         Width           =   2055
      End
      Begin VB.TextBox txtID 
         Height          =   375
         Left            =   840
         TabIndex        =   1
         Top             =   1560
         Width           =   1695
      End
      Begin VB.Label lblID 
         Caption         =   "ID Comprobante"
         Height          =   255
         Left            =   1080
         TabIndex        =   19
         Top             =   1200
         Width           =   1335
      End
      Begin VB.Label lblAyuda3 
         Caption         =   "Anula y genera un comprobante, mediante identificacion automatica."
         Height          =   495
         Left            =   480
         TabIndex        =   17
         Top             =   480
         Width           =   2655
      End
   End
   Begin VB.Frame Conta1 
      Caption         =   "Contabilizacion Individual"
      Height          =   4335
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   8535
      Begin VB.CommandButton cmdContabiliza1 
         BackColor       =   &H0000C000&
         Caption         =   "Contabiliza"
         Height          =   495
         Left            =   6120
         MaskColor       =   &H0000C000&
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   3000
         Width           =   2175
      End
      Begin VB.TextBox txtCod2 
         Height          =   375
         Left            =   3600
         TabIndex        =   20
         Top             =   2520
         Width           =   2055
      End
      Begin VB.TextBox txtCod1 
         Height          =   375
         Left            =   720
         TabIndex        =   18
         Top             =   2520
         Width           =   1935
      End
      Begin VB.Frame Frame2 
         Caption         =   "Ingreso"
         Height          =   1815
         Left            =   3360
         TabIndex        =   11
         Top             =   360
         Width           =   4935
         Begin VB.OptionButton op5 
            Caption         =   "Almacen Salidas"
            Height          =   375
            Left            =   2040
            TabIndex        =   9
            Top             =   1080
            Width           =   1215
         End
         Begin VB.OptionButton op4 
            Caption         =   "Almacen Ingresos"
            Height          =   375
            Left            =   2040
            TabIndex        =   8
            Top             =   360
            Width           =   1215
         End
         Begin VB.OptionButton op3 
            Caption         =   "Cobranzas"
            Height          =   375
            Left            =   240
            TabIndex        =   7
            Top             =   1320
            Width           =   1335
         End
         Begin VB.OptionButton op2 
            Caption         =   "Facturacion"
            Height          =   375
            Left            =   240
            TabIndex        =   6
            Top             =   840
            Width           =   1215
         End
         Begin VB.OptionButton op1 
            Caption         =   "Contratos"
            Height          =   375
            Left            =   240
            TabIndex        =   5
            Top             =   360
            Value           =   -1  'True
            Width           =   1095
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Tipo"
         Height          =   855
         Left            =   360
         TabIndex        =   10
         Top             =   360
         Width           =   2655
         Begin VB.OptionButton opt2 
            Caption         =   "Egreso"
            Height          =   495
            Left            =   1440
            TabIndex        =   4
            Top             =   240
            Width           =   975
         End
         Begin VB.OptionButton opt1 
            Caption         =   "Ingreso"
            Height          =   495
            Left            =   240
            TabIndex        =   3
            Top             =   240
            Value           =   -1  'True
            Width           =   975
         End
      End
      Begin VB.Label lblAyuda2 
         Caption         =   "Ayuda2"
         Height          =   375
         Left            =   3120
         TabIndex        =   15
         Top             =   3600
         Width           =   2535
      End
      Begin VB.Label lblCod2 
         Caption         =   "Label Cod 2"
         Height          =   375
         Left            =   3720
         TabIndex        =   14
         Top             =   3000
         Width           =   1815
      End
      Begin VB.Label lblAyuda1 
         Caption         =   "Ayuda1"
         Height          =   375
         Left            =   480
         TabIndex        =   13
         Top             =   3600
         Width           =   2055
      End
      Begin VB.Label lblCod1 
         Caption         =   "Label Cod 1"
         Height          =   375
         Left            =   720
         TabIndex        =   12
         Top             =   3000
         Width           =   1815
      End
   End
End
Attribute VB_Name = "FrmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdContabiliza1_Click()
    'Contabilizacion individual
    If txtCod1.Enabled Then
        cod1 = CLng(txtCod1.Text)
    Else
        cod1 = 0
    End If
    If txtCod2.Enabled Then
        cod2 = CLng(txtCod2.Text)
    Else
        cod2 = 0
    End If
    ' Contabiliza segun sea el caso
    Call ContabilizadorMaestro(cod1, cod2)
End Sub

Private Sub cmdContabiliza2_Click()
    'Anulacion y generacion
    Dim CompId As Long
    Dim result As Integer
    Dim query_data As String
    Dim rs_data99 As New ADODB.Recordset
    'Asignacion de valores
    CompId = CLng(txtID.Text)
    query_data = "SELECT idCAutom, cod1, cod2 FROM tblComp WHERE CompId = " & CompId
    If rs_data99.State = adStateOpen Then rs_data99.Close
    Set rs_data99 = GetSelect2(query_data)
    If rs_data99.RecordCount > 0 Then
        rs_data99.MoveFirst
        nro_operacion = CInt(rs_data99!idCAutom)
        cod1 = CLng(rs_data99!cod1)
        cod2 = CLng(rs_data99!cod2)
    Else
        MsgBox "No se encontro ningun registro.", vbInformation, "Informacion"
        Exit Sub
    End If
    If rs_data99.State = adStateOpen Then rs_data99.Close
    'Solicitud de confirmacion
    result = MsgBox("�Esta seguro de anular y generar el/los comprobante(s)?", vbQuestion + vbOKCancel, "Soporte")
    If result = vbOK Then
        'Anulacion
        Call ExecProcedure("[dbo].[conta_anula_autom_comprobantes] " & CompId)
        'Generacion
        Call ContabilizadorMaestro(cod1, cod2)
    Else
        Exit Sub
    End If
End Sub

Private Sub Form_Load()
    Call CargarMain
    Call opt1_Click
    Call CargarLabel(Me)
End Sub

Private Sub op1_Click()
    'Ingresos - Contratos
    Call CargarLabel(Me)
End Sub

Private Sub op2_Click()
    'Ingresos - Facturacion
    Call CargarLabel(Me)
End Sub

Private Sub op3_Click()
    'Ingresos - Cobranzas
    Call CargarLabel(Me)
End Sub

Private Sub op4_Click()
    'Egresos - Almacen Ingreso
    Call CargarLabel(Me)
End Sub

Private Sub op5_Click()
    'Egresos - Almacen Salida
    Call CargarLabel(Me)
End Sub

Private Sub opt1_Click()
    'Caso ingreso
    Frame2.Caption = "Ingreso"
    'Son ingresos
    op1.Visible = True
    op2.Visible = True
    op3.Visible = True
    'Son egresos
    op4.Visible = False
    op5.Visible = False
    'Valor por defecto
    op1.Value = True
    'XXX
    Call CargarLabel(Me)
End Sub

Private Sub opt2_Click()
    'Caso egreso
    Frame2.Caption = "Egreso"
    'Son ingresos
    op1.Visible = False
    op2.Visible = False
    op3.Visible = False
    'Son egresos
    op4.Visible = True
    op5.Visible = True
    ' Valor por defecto
    op4.Value = True
End Sub

Public Sub CargarLabel(ByVal frm As FrmMain)
    Const ND As String = "No definido"
    Dim labeldb As New ADODB.Recordset
    Dim nro_op As Integer
    Dim query As String
    nro_op = 0
    'Determinacion del caso de operacion
    ' Caso Ingreso
    If opt1.Value Then
        If op1.Value Then
            ' Contratos
            nro_op = 1
        ElseIf op2.Value Then
            ' Facturacion
            nro_op = 2
        ElseIf op3.Value Then
            ' Cobranzas
            nro_op = 3
        Else
            ' No definido
            nro_op = -1
        End If
    ' Caso Egreso
    ElseIf opt2.Value Then
        If op4.Value Then
            'Almacen ingreso
            nro_op = 4
        ElseIf op5.Value Then
            'Almacen egreso
            nro_op = 5
        Else
            'No definido
            nro_op = -2
        End If
    ' Caso No definido
    Else
        nro_op = 0
    End If
    If nro_op > 0 Then
        query = "SELECT IIF(LEN(cod1_name) > 1, cod1_name, '') as cod1_name, " & _
                "CONCAT('Columna: ', cod1_columna, ' Tabla: ', cod1_tabla) AS ayuda1, " & _
                "IIF(LEN(cod1_name) > 1, CONVERT(BIT, 1), CONVERT(BIT, 0)) AS enabled1, " & _
                "IIF(LEN(cod2_name) > 1, cod2_name, '') as cod2_name, " & _
                "CASE " & _
                    "WHEN LEN(cod2_columna) > 1 AND LEN(cod2_tabla) > 1 THEN CONCAT('Columna: ', cod2_columna, ' Tabla: ', cod2_tabla) " & _
                    "ELSE ''" & _
                "END as ayuda2, " & _
                "IIF(LEN(cod2_name) > 1, CONVERT(BIT, 1), CONVERT(BIT, 0)) AS enabled2 " & _
                "FROM tblCAutomatico WHERE idCAutom = " & nro_op
        If labeldb.State = adStateOpen Then labeldb.Close
        Set labeldb = GetSelect(query)
        lblCod1.Caption = labeldb.Fields(0)
        lblAyuda1.Caption = labeldb.Fields(1)
        txtCod1.Enabled = labeldb.Fields(2)
        txtCod1.Visible = labeldb.Fields(2)
        lblCod2.Caption = labeldb.Fields(3)
        lblAyuda2.Caption = labeldb.Fields(4)
        txtCod2.Enabled = labeldb.Fields(5)
        txtCod2.Visible = labeldb.Fields(5)
    Else
        lblCod1.Caption = ND
        lblAyuda1.Caption = ND
        txtCod1.Enabled = False
        txtCod1.Visible = False
        lblCod2.Caption = ND
        lblAyuda2.Caption = ND
        txtCod2.Enabled = False
        txtCod2.Visible = False
    End If
    nro_operacion = nro_op
End Sub

